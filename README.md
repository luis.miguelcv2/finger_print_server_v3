# finger_print_server_v3
Digital Person OneTouch

## Requeriments
  * [Maven](https://maven.apache.org/install.html)
  * Java 1.8


## How to start

In order to start the seed use:

```bash
$ git clone https://gitlab.com/j_rico/finger_print_server_v3
$ cd finger_print_server_v3
$ mvn clean install
$ mvn spring-boot:run
```

## Build an executable JAR
```bash
$ mvn clean package
$ java -jar target/finger_print_server_v3-{version}.jar 

#opcional change port jar
$ java -Dserver.port={PORT} -jar target/finger_print_server_v3-{version}.jar 
```


Now open your browser at:

  * http://localhost:8080

## Contributors
[<img alt="LuisColmenarez" src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/3625714/avatar.png?width=90" width="117">](https://gitlab.com/luis.miguelcv2)
