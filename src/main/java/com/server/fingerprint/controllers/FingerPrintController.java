package com.server.fingerprint.controllers;

import java.util.Map;

import com.server.fingerprint.Constants;
import com.server.fingerprint.common.Utils;
import com.server.fingerprint.configuration.SimpleRunnable;
import com.server.fingerprint.services.FingerPrintService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/** 
 * Class for customer requests.
 * @author luis.colmenarez
 */
@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*", methods = { RequestMethod.GET })
public class FingerPrintController {

    private final static Logger LOGGER = LogManager.getLogger(FingerPrintController.class);

    @Autowired
    private FingerPrintService service;

    @RequestMapping(value = "getStatusServer", method = RequestMethod.GET)
    public String statusServer() {
        return service.statusServer();
    }

    @RequestMapping(value = "numHuellas", method = RequestMethod.GET)
    public String numbFingerPrints(@RequestParam Map<String, String> request) {
        return service.numbFingerPrints(request);
    }

    @RequestMapping(value = "sendHuella", method = RequestMethod.GET)
    public String sendFingerPrint(@RequestParam Map<String, String> request) {
        return service.sendFingerPrint(request);
    }

    @RequestMapping(value = "getStatusLector", method = RequestMethod.GET)
    public String statusReader(@RequestParam Map<String, String> request) {
        String response = Constants.CHAR_NULL;
        SimpleRunnable runnable;
        Thread thread = new Thread(runnable = new SimpleRunnable(response) {
            @Override
            public void run() {
                this.setMessage(service.statusReader(request));
            }
        });

        try {
            thread.start();
            thread.join(Constants.SECONDS_TIME_OUT);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }

        response = runnable.getMessage();
        if (response.equals(Constants.CHAR_NULL)) {
            thread.stop();
            service.unlockServer();
            response = Utils.strResponse(Constants.SUCESS, Constants.WAIT_FINGERPRINT, null);
        }

        return response;
    }

    @RequestMapping(value = "esperarProc", method = RequestMethod.GET)
    public String waitProcess() {
        return service.waitProcess();
    }

    @RequestMapping(value = "startEnrollment", method = RequestMethod.GET)
    public String startEnrollment() {
        return service.startEnrollment();
    }
}