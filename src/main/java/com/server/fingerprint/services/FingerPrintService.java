package com.server.fingerprint.services;

import java.util.Map;

/**
 * Methods for the use of fingerprint process management
 * 
 * @author luis.colmenarez
 */
public interface FingerPrintService {

    /**
     * Check the current status of the server.
     * 
     * @return String
     */
    String statusServer();

    /**
     * Check a set of fingerprints compared to a CSV file
     * 
     * @return String
     */
    String numbFingerPrints(Map<String, String> request);

    /**
     * Load fingerprints into a CSV file and delete the remaining ones.
     * 
     * @return String
     */
    String sendFingerPrint(Map<String, String> request);

    /**
     * Activate modo enroll for captured a fingerprint in high definition
     * 
     * @return String
     */
    String startEnrollment();

    /**
     * Capture a fingerprint in a certain time range
     * 
     * @return String
     */
    String statusReader(Map<String, String> request);

    /**
     * Verify the existence of a captured fingerprint
     * 
     * @return String
     */
    String waitProcess();

    /**
     * Blocks access to the server
     */
    void lockServer();

    /**
     * Unlock access to the server.
     */
    void unlockServer();
}