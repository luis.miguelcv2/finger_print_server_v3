package com.server.fingerprint.services.impl;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPFingerIndex;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.readers.DPFPReadersCollection;
import com.server.fingerprint.Constants;
import com.server.fingerprint.common.Utils;
import com.server.fingerprint.models.UserDatabase;
import com.server.fingerprint.models.UserDatabase.User;
import com.server.fingerprint.models.factory.SessionUserDatabaseFactory;
import com.server.fingerprint.services.AsyncService;
import com.server.fingerprint.services.FingerPrintService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Class that implements the processes of FingerPrintService
 * 
 * @author luis.colmenarez
 */
@Service
public class FingerPrintServiceImpl implements FingerPrintService {

    private final static Logger LOGGER = LogManager.getLogger(FingerPrintServiceImpl.class);

    @Autowired
    private AsyncService async;

    private boolean wait = true;
    private UserDatabase userDB;
    private Map<String, List<DPFPFingerIndex>> nameDB;
    private User user;

    private Map<String, String[]> hashRequest;
    private Map<String, String[]> hashLoad;

    private String activeReader = null;

    private DPFPSample sample;

    private long clock;

    private StringBuilder fingerPrint;

    private DPFPFingerIndex finger = null;
    private DPFPFeatureExtraction featureExtractor = null;
    private DPFPEnrollment enrollment = null;

    private boolean isEnrollment;
    private boolean capturing;
    private boolean verifFP = true;

    private static final EnumMap<DPFPFingerIndex, String> fingerNames;

    static {
        fingerNames = new EnumMap<DPFPFingerIndex, String>(DPFPFingerIndex.class);
        fingerNames.put(DPFPFingerIndex.LEFT_PINKY, "left pinky");
        fingerNames.put(DPFPFingerIndex.LEFT_RING, "left ring");
        fingerNames.put(DPFPFingerIndex.LEFT_MIDDLE, "left middle");
        fingerNames.put(DPFPFingerIndex.LEFT_INDEX, "left index");
        fingerNames.put(DPFPFingerIndex.LEFT_THUMB, "left thumb");
        fingerNames.put(DPFPFingerIndex.RIGHT_PINKY, "right pinky");
        fingerNames.put(DPFPFingerIndex.RIGHT_RING, "right ring");
        fingerNames.put(DPFPFingerIndex.RIGHT_MIDDLE, "right middle");
        fingerNames.put(DPFPFingerIndex.RIGHT_INDEX, "right index");
        fingerNames.put(DPFPFingerIndex.RIGHT_THUMB, "right thumb");
    }

    @Override
    public String statusServer() {
        return !this.isLock() ? Utils.strResponse(Constants.SUCESS, Constants.WAIT_NUM_FINGERPRINT, null)
                : Utils.strResponse(Constants.SUCESS, Constants.SERVER_BUSY, null);
    }

    @Override
    public String numbFingerPrints(Map<String, String> request) {
        String response = Utils.strResponse(Constants.ERROR, Constants.ERROR_LOG, null);
        if (!this.isLock()) {
            this.lockServer();
            Map<String, String[]> hashServer = new HashMap<String, String[]>();
            Map<String, String[]> hashFile;

            try {
                this.hashLoad = new HashMap<String, String[]>();
                String totalFP = request.get(Constants.NUM).toString();
                String hashStr = request.get(Constants.HASH).toString();

                int countFP = 0;
                for (String key : hashStr.split(Constants.CHAR_COMMA)) {
                    hashServer.put(key, null);
                    countFP++;
                }

                if (Integer.valueOf(totalFP) == countFP) {
                    try {
                        this.userDB = this.createDB();
                        this.nameDB = new HashMap<String, List<DPFPFingerIndex>>();
                    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                        throw new Exception("Database: " + ex);
                    }

                    hashFile = Utils.readCSVfile(Constants.PATH_FILE_CSV);
                    this.hashRequest(hashFile, hashServer);

                    String strRequest = "[";
                    for (Object key : this.hashRequest.keySet()) {
                        strRequest += "\"" + key.toString() + "\",";
                    }

                    String aux_ = strRequest.substring(strRequest.length() - 1);
                    if (aux_.equals(Constants.CHAR_COMMA)) {
                        strRequest = strRequest.substring(0, strRequest.length() - 1);
                    }

                    strRequest += "]";
                    response = Utils.strResponse(Constants.SUCESS, Constants.WAIT_FINGERPRINT, strRequest);
                } else {
                    response = Utils.strResponse(Constants.ERROR, Constants.FINGERPRINT_NOT_MATCH, null);
                }

            } catch (Exception e) {
                LOGGER.error(e);
            }
            this.unlockServer();
        }
        return !this.isLock() ? response : Utils.strResponse(Constants.SUCESS, Constants.SERVER_BUSY, null);
    }

    @Override
    public String sendFingerPrint(Map<String, String> request) {
        String response = Utils.strResponse(Constants.ERROR, Constants.ERROR_LOG, null);
        if (!this.isLock()) {
            this.lockServer();
            String[] nssFmd = new String[2];
            String hash = String.valueOf(request.get(Constants.HASH));
            nssFmd[0] = String.valueOf(request.get(Constants.NSS));
            nssFmd[1] = String.valueOf(request.get(Constants.FMD));

            if (hash != "" && nssFmd[0] != "" && nssFmd[1] != "") {
                this.hashLoad.put(hash, nssFmd);
                this.hashRequest.remove(hash);
            }

            if (this.hashRequest.size() == 0) {
                Utils.insertDB(this.nameDB, this.userDB, this.hashLoad);

                if ((hash != "" && hash != null)) {
                    Utils.writeCSVfile(Constants.PATH_FILE_CSV, this.hashLoad);
                }

                response = Utils.strResponse(Constants.SUCESS, Constants.OK, null);
            } else {
                response = Utils.strResponse(Constants.SUCESS, Constants.NEXT,
                        "\"" + this.hashRequest.entrySet().iterator().next().getKey() + "\"");
            }
            this.unlockServer();
        }
        return !this.isLock() ? response : Utils.strResponse(Constants.SUCESS, Constants.SERVER_BUSY, null);
    }

    @Override
    public String startEnrollment() {
        String response = Utils.strResponse(Constants.SUCESS, Constants.ENROLL_ACTIVED, null);
        if (!this.isLock()) {
            this.finger = DPFPFingerIndex.RIGHT_INDEX;
            this.featureExtractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
            this.enrollment = DPFPGlobal.getEnrollmentFactory().createEnrollment();
            this.isEnrollment = true;
            this.verifFP = true;
            this.capturing = false;
        }
        return !this.isLock() ? response : Utils.strResponse(Constants.SUCESS, Constants.SERVER_BUSY, null);
    }

    @Override
    public String statusReader(Map<String, String> request) {
        String response = Utils.strResponse(Constants.ERROR, Constants.ERROR_LOG, null);
        if (!this.isLock()) {
            this.lockServer();

            if (this.selectReader() == null && this.activeReader == null) {
                response = Utils.strResponse(Constants.ERROR, Constants.READER_NOT_FOUND, null);
            } else {
                if (this.isEnrollment && !this.verifFP) {
                    this.enrollment();
                    response = Utils.strResponse(Constants.SUCESS, Constants.PROCESSING,
                            "\"" + this.toImage(this.sample, Constants.JPG) + "\"");
                } else {
                    try {
                        this.sample = Utils.getSample(this.activeReader, Constants.WAIT_FINGERPRINT + ".\n");

                        if (this.sample == null) {
                            response = Utils.strResponse(Constants.ERROR, Constants.WAIT_FINGERPRINT, null);
                        } else {
                            String imgstr = this.toImage(sample, Constants.JPG);
                            response = Utils.strResponse(Constants.SUCESS, Constants.PROCESSING, "\"" + imgstr + "\"");
                        }
                    } catch (InterruptedException e) {
                        LOGGER.info(Constants.TIMEOUT_REQUEST);
                    }
                }
            }
            this.unlockServer();
        }
        return !this.isLock() ? response : Utils.strResponse(Constants.SUCESS, Constants.SERVER_BUSY, null);
    }

    @Override
    public String waitProcess() {
        String response = Utils.strResponse(Constants.ERROR, Constants.ERROR_LOG, null);
        if (!this.isLock()) {
            this.lockServer();
            if (this.fingerPrint == null) {
                try {
                    if (this.capturing) {
                        response = this.isEnrollment ? Utils.strResponse(Constants.SUCESS, Constants.ENTER_FINGER, null)
                                : Utils.strResponse(Constants.SUCESS, Constants.FINGERPRINT_UNIDENTIFIED, null);
                        this.verifFP = false;
                    } else {
                        this.user = this.existPerson();
                        if (this.user != null) {
                            response = this.isEnrollment
                                    ? Utils.strResponse(Constants.ERROR, Constants.FINGERPRINT_EXIST, null)
                                    : Utils.strResponse(Constants.SUCESS, Constants.FINGERPRINT_VERIFIED,
                                            "\"" + this.user.getUsername() + "\"");
                        } else {
                            response = this.isEnrollment
                                    ? Utils.strResponse(Constants.SUCESS, Constants.ENTER_FINGER, null)
                                    : Utils.strResponse(Constants.SUCESS, Constants.FINGERPRINT_UNIDENTIFIED, null);
                            this.verifFP = false;
                        }
                    }

                } catch (Exception e) {
                    response = Utils.strResponse(Constants.ERROR, Constants.ERROR_LOG, null);
                    LOGGER.error(e);
                }
            } else {
                response = Utils.strResponse(Constants.SUCESS, Constants.FINGERPRINT_CAPTURED,
                        "\"" + this.fingerPrint + "\"");
                this.fingerPrint = null;
                this.capturing = false;
            }

            this.unlockServer();
        }

        return !this.isLock() ? response : Utils.strResponse(Constants.SUCESS, Constants.SERVER_BUSY, null);

    }

    @Override
    public void lockServer() {
        this.clock = System.currentTimeMillis();
        this.wait = false;
    }

    @Override
    public void unlockServer() {
        this.wait = true;
    }

    private String enrollment() {
        String response = Constants.CHAR_NULL;
        try {
            if (this.enrollment != null) {
                if (this.enrollment.getFeaturesNeeded() > 0) {
                    this.fingerPrint = null;
                    this.sample = Utils.getSample(this.activeReader,
                            String.format("1Scan your %s finger (%d remaining)\n", fingerName(finger),
                                    this.enrollment.getFeaturesNeeded()));

                    if (this.sample != null) {
                        DPFPFeatureSet featureSet = null;
                        try {
                            featureSet = featureExtractor.createFeatureSet(sample,
                                    DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);

                        } catch (DPFPImageQualityException e) {
                            LOGGER.info("Bad image quality: \"%s\". Try again. \n", e.getCaptureFeedback().toString());
                        }

                        this.enrollment.addFeatures(featureSet);
                    }
                } else {
                    this.fingerPrint = new StringBuilder();
                    DPFPTemplate template = enrollment.getTemplate();
                    byte[] huella = template.serialize();

                    for (int i = 0; i < huella.length; i++) {
                        this.fingerPrint.append("i" + huella[i]);
                    }
                    this.isEnrollment = false;
                }
            }

        } catch (InterruptedException | DPFPImageQualityException e) {
            LOGGER.error(e);
            response = Utils.strResponse(Constants.ERROR, Constants.ERROR_LOG, null);
        }
        return response;
    }

    private String toImage(DPFPSample sample, String type) {
        // Cast fingerprint to image
        Image src = DPFPGlobal.getSampleConversionFactory().createImage(sample);
        BufferedImage image = Utils.toBufferedImage(src);
        BufferedImage SubImgage = image.getSubimage(140, 100, 300, 400);

        // creates output image
        BufferedImage outputImage = new BufferedImage(150, 200, SubImgage.getType());

        // scales the input image to the output image
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(SubImgage, 0, 0, 150, 200, null);
        g2d.dispose();

        return Utils.encodeToString(outputImage, type);
    }

    private User existPerson() throws Exception {
        CompletableFuture<User> user = null;
        User response = null;
        try {
            async.setFlagMatch(false);
            // if (this.userDB.getSize() > Constants.MAX_PROCESS_THREAD) {
            // final UserDatabase dbOne = this.userDB;

            // CompletableFuture<User> threadOne = async.verifyFingerprint(this.sample,
            // dbOne);
            // CompletableFuture.allOf(threadOne).join();
            // user = threadOne;
            // } else {
            // user = async.verifyFingerprint(this.sample, this.userDB);
            // CompletableFuture.allOf(user).join();
            // }

            user = async.verifyFingerprint(this.sample, this.userDB);
            CompletableFuture.allOf(user).join();

            response = user != null && user.get() != null ? user.get() : null;

        } catch (InterruptedException e) {
            throw new Exception("Verify fingerprint: " + e);
        } catch (NullPointerException n) {
            response = null;
        } catch (ExecutionException i) {
            i.printStackTrace();
        }
        return response;
    }

    private boolean isLock() {
        return !this.wait && (System.currentTimeMillis() - this.clock) <= Constants.MINUTES_TIME_OUT;
    }

    private void hashRequest(Map<String, String[]> hashCsv, Map<String, String[]> hashServer) {
        this.hashRequest = hashServer;
        for (Object key : hashCsv.keySet()) {
            if (hashServer.containsKey(key)) {
                this.hashRequest.remove(key);
                this.hashLoad.put(key.toString(), hashCsv.get(key));
            }
        }
    }

    private String selectReader() {
        String response = null;
        try {
            DPFPReadersCollection readers = DPFPGlobal.getReadersFactory().getReaders();

            if (readers == null || readers.isEmpty()) {
                this.activeReader = null;
            } else {
                this.activeReader = readers.get(0).getSerialNumber();
                response = readers.get(0).getSerialNumber();
            }
        } catch (Exception e) {
            LOGGER.error("Error buscando el lector");
            this.activeReader = null;
        }
        return response;
    }

    private UserDatabase createDB() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        UserDatabase.Factory dbFactory;
        dbFactory = (UserDatabase.Factory) Class.forName(SessionUserDatabaseFactory.class.getName()).newInstance();
        return dbFactory.createDB();
    }

    private String fingerName(DPFPFingerIndex finger) {
        return fingerNames.get(finger);
    }
}