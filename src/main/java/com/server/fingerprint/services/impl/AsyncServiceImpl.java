package com.server.fingerprint.services.impl;

import java.util.concurrent.CompletableFuture;

import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import com.server.fingerprint.models.UserDatabase;
import com.server.fingerprint.models.UserDatabase.User;
import com.server.fingerprint.services.AsyncService;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * Class that implements the processes of FingerPrintService
 * 
 * @author luis.colmenarez
 */
@Service
public class AsyncServiceImpl implements AsyncService {

    private boolean flagMatch;

    @Override
    @Async("asyncExecutor")
    public CompletableFuture<User> verifyFingerprint(final DPFPSample sample, final UserDatabase db)
            throws InterruptedException {

        DPFPFeatureExtraction featureExtractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
        DPFPFeatureSet featureSet;
        User us = null;
        try {
            featureSet = featureExtractor.createFeatureSet(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);
            DPFPVerification matcher = DPFPGlobal.getVerificationFactory().createVerification();
            matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);
            for (Object user : db.getAll()) {
                for (DPFPTemplate template : ((User) user).getAllTemplate()) {
                    if (template != null) {
                        if (!this.flagMatch) {
                            if (matcher.verify(featureSet, template).isVerified()) {
                                this.flagMatch = true;
                                return CompletableFuture.completedFuture((User) user);
                            }
                        } else {
                            return CompletableFuture.completedFuture(us);
                        }
                    }
                }
            }
        } catch (DPFPImageQualityException ex) {
            throw new InterruptedException(ex.getMessage());
        }
        return CompletableFuture.completedFuture(us);
    }

    @Override
    public boolean getFlagMatch() {
        return this.flagMatch;
    }

    @Override
    public void setFlagMatch(final boolean flagMatch) {
        this.flagMatch = flagMatch;
    }
}