package com.server.fingerprint.services;

import java.util.concurrent.CompletableFuture;

import com.digitalpersona.onetouch.DPFPSample;
import com.server.fingerprint.models.UserDatabase;
import com.server.fingerprint.models.UserDatabase.User;

/**
 * Asynchronous methods for the use of fingerprint process management
 * @author luis.colmenarez
 */
public interface AsyncService {

        CompletableFuture<User> verifyFingerprint(final DPFPSample sample, final UserDatabase db)
                        throws InterruptedException;

        boolean getFlagMatch();

        void setFlagMatch(final boolean flagMatch);
}