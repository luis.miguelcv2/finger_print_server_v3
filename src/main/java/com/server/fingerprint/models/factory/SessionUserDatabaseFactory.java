package com.server.fingerprint.models.factory;

import com.digitalpersona.onetouch.DPFPFingerIndex;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.server.fingerprint.models.UserDatabase;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Implementation UserDatabase.Factory interface that produce instances of
 * in-memory user database without serialization.
 */
public class SessionUserDatabaseFactory implements UserDatabase.Factory {

    @Override
    public UserDatabase createDB() {
        return new SessionUserDatabase();
    }

    /**
     * In-memory user database without serialization
     */
    private static class SessionUserDatabase implements UserDatabase {
        private Map<Object, Object> db = new HashMap<Object, Object>();

        /**
         * Adds a user to the database
         *
         * @param username user to be added
         * @return user added
         */
        @Override
        public User addUser(String username) {
            User ret = new UserImpl(username);
            this.db.put(username, ret);
            return ret;
        }

        @Override
        public void addUser(User u) {
            this.db.put(u.getUsername(), u);
        }

        /**
         * Finds a user by name
         *
         * @param username user to find
         * @return user found, or <code>null</code> if not found)
         */
        @Override
        public User getUser(String username) {
            return (User) this.db.get(username);
        }

        @Override
        public int getSize() {
            return this.db.size();
        }

        @Override
        public Collection<Object> getAll() {
            return this.db.values();
        }

        @Override
        public Map<Object, Object> reverseDB() {
            return this.db.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
        }

        /**
         * In-memory implementation of UserImpl interface
         */
        private static class UserImpl implements User {
            private String username;
            private EnumMap<DPFPFingerIndex, DPFPTemplate> templates;

            public UserImpl(String username) {
                this.username = username;
                templates = new EnumMap<DPFPFingerIndex, DPFPTemplate>(DPFPFingerIndex.class);
            }

            @Override
            public String getUsername() {
                return username;
            }

            @Override
            public DPFPTemplate getTemplate(DPFPFingerIndex finger) {
                return templates.get(finger);
            }

            @Override
            public void setTemplate(DPFPFingerIndex finger, DPFPTemplate template) {
                templates.put(finger, template);
            }

            @Override
            public boolean isEmpty() {
                return templates.isEmpty();
            }

            @Override
            public Collection<DPFPTemplate> getAllTemplate() {
                return templates.values();
            }
        }
    }
}
