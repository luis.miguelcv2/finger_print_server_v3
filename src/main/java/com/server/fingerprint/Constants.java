package com.server.fingerprint;

/**
 * Constants of the application
 * 
 * @author luis.colmenarez
 */
public class Constants {

    public static final long MINUTE = 60000; // Millis
    public static final long MINUTES_TIME_OUT = MINUTE * 3;
    public static final long SECOND = 1000; // Millis
    public static final long SECONDS_TIME_OUT = SECOND * 8;
    public static final long MAX_PROCESS_THREAD = 10;
    public static final String TIME_OUT = "time out";

    public static final String ERROR_LOG = "Error, consulte el log del servidor";
    public static final String ERROR_TYPE = "Error tipo";
    public static final String ERROR = "0";
    public static final String SUCESS = "1";
    public static final String PROCESSING = "procesando";

    public static final String ENROLL_ACTIVED = "Modo enroll activo";
    public static final String WAIT_FINGERPRINT = "Esperando huella";
    public static final String FINGERPRINT_EXIST = "Huella ya existe";
    public static final String FINGERPRINT_PROCESS = "Huellas Procesadas";
    public static final String FINGERPRINT_VERIFIED = "Huella verificada";
    public static final String FINGERPRINT_UNIDENTIFIED = "Huella no identificada";
    public static final String FINGERPRINT_NOT_MATCH = "El numero de huellas no coinciden";
    public static final String NEXT = "next";
    public static final String OK = "ok";
    public static final String ERROR_NSS = "Error al insertar dedo a NSS";
    public static final String WAIT_NUM_FINGERPRINT = "Esperando num huella";
    public static final String READER_NOT_FOUND = "Lector no encontrado";
    public static final String READER_CONNECT = "Reader is connected";
    public static final String READER_DISCONNECT = "Reader is disconnected";

    // SERVER
    public static final String SERVER_BUSY = "Servidor ocupado";

    // CHARACTERS
    public static final String CHAR_COMMA = ",";
    public static final String CHAR_NULL = "";

    public static final String PATH_FILE_CSV = "export_fmd.csv";
    public static final String HASH = "hash";
    public static final String NUM = "num";
    public static final String FMD = "fmd";
    public static final String NSS = "nss";

    public static final String PNG = "png";
    public static final String JPG = "jpg";
    public static final String DATA_PNG = "data:image/png;base64,";
    public static final String DATA_JPG = "data:image/jpeg;base64,";
    public static final String SCAN_FINGER = "Scan your finger";
    public static final String ENTER_FINGER = "Ingresa un dedo";
    public static final String ENTER_SAME_FINGER = "Ingresa mismo dedo";
    public static final String FINGERPRINT_CAPTURED = "Huella capturada";

    // LOGS
    public static final String READING_FILE_CSV = "Leyendo Archivo CSV";
    public static final String INSERTING_DB = "Insertando en Base de Datos";
    public static final String WRITING_FILE_CSV = "Escribiendo en el archivo CSV";
    public static final String TIMEOUT_REQUEST = "Tiempo del Request excedido, culminando thread";
    public static final String CASTING_IMAGE_FINGERPRINT = "Conviertiendo huella a imagen";
    public static final String CAPTURING_FINGERPRINT = "Capturando Huella";
    public static final String SEARCHING_MATCH = "Buscando Coincidencias";

}