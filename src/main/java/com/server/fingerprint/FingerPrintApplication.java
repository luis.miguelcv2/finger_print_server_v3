package com.server.fingerprint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class of start-up.
 * @author luis.colmenarez
 */
@SpringBootApplication
public class FingerPrintApplication {
	public static void main(String[] args) {
		SpringApplication.run(FingerPrintApplication.class, args);
	}
}
