package com.server.fingerprint.configuration;

import java.util.concurrent.Executor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * Configuration of asynchronous processes
 * @author luis.colmenarez
 */
@Configuration
@EnableAsync
public class SpringAsyncConfig {

    @Value("${executor.poolsize}")
    private String poolSize;

    @Value("${executor.maxpoolsize}")
    private String maxPoolSize;

    @Value("${executor.queuecapacity}")
    private String queueCapacity;

    @Value("${executor.prefix}")
    private String prefix;

    @Value("${executor.timeout}")
    private String timeOut;

    @Bean(name = "asyncExecutor")
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(Integer.parseInt(this.poolSize));
        executor.setMaxPoolSize(Integer.parseInt(this.maxPoolSize));
        executor.setQueueCapacity(Integer.parseInt(this.queueCapacity));
        executor.setAllowCoreThreadTimeOut(true);
        executor.setAwaitTerminationSeconds(Integer.parseInt(this.timeOut));
        executor.setThreadNamePrefix(this.prefix);
        executor.initialize();
        return executor;
    }
}
