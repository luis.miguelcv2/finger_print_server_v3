package com.server.fingerprint.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class of runnable processes
 * @author luis.colmenarez
 */
public class SimpleRunnable implements Runnable {

    private final static Logger LOGGER = LogManager.getLogger(SimpleRunnable.class);

    private String message;

    public SimpleRunnable(final String message) {
        this.message = message;
    }

    @Override
    public void run() {
        LOGGER.info(message);
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }
}
