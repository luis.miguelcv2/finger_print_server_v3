package com.server.fingerprint.common;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import javax.imageio.ImageIO;

import com.digitalpersona.onetouch.DPFPFingerIndex;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.DPFPCapturePriority;
import com.digitalpersona.onetouch.capture.event.DPFPDataEvent;
import com.digitalpersona.onetouch.capture.event.DPFPDataListener;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusAdapter;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusEvent;
import com.server.fingerprint.Constants;
import com.server.fingerprint.models.UserDatabase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class of useful methods for fingerprint processes.
 * @author luis.colmenarez
 */
public class Utils {

    private static Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    public static String strResponse(final String status, final String message, final String data) {
        return "{\"s\":" + status + ", \"m\":\"" + message + "\",\"d\":" + data + "}";
    }

    public static Map<String, String[]> readCSVfile(final String path) throws IOException {
        Map<String, String[]> hash = new HashMap<String, String[]>();
        String line = Constants.CHAR_NULL;
        BufferedReader buff = new BufferedReader(new FileReader(path));
        line = buff.readLine();
        while (line != null && !line.equals(Constants.CHAR_NULL)) {
            String[] nssFmd = new String[2];
            String[] lineAux = line.split(Constants.CHAR_COMMA);
            nssFmd[0] = lineAux[0].replace("\"", Constants.CHAR_NULL); // NSS
            nssFmd[1] = lineAux[2].replace("\"", Constants.CHAR_NULL); // fmd
            hash.put(lineAux[1].replace("\"", Constants.CHAR_NULL), nssFmd);
            line = buff.readLine();
        }
        buff.close();
        return hash;
    }

    public static void writeCSVfile(String path, Map<String, String[]> hashLoad) {
        try {
            FileOutputStream fos = new FileOutputStream(path);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

            for (Object key : hashLoad.keySet()) {
                String hash = key.toString();
                String nss = hashLoad.get(key)[0];
                String fmd = hashLoad.get(key)[1];

                bw.write(nss + "," + hash + "," + fmd);
                bw.newLine();
            }
            bw.close();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void clearCSVfile(String path) throws IOException {
        BufferedWriter buffW = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)));
        buffW.write(Constants.CHAR_NULL);
        buffW.newLine();
        buffW.close();
    }

    public static void insertDB(Map<String, List<DPFPFingerIndex>> nameDB, UserDatabase userDB,
            Map<String, String[]> hashLoad) {
        List<DPFPFingerIndex> fingerIndex = null;

        for (Object key : hashLoad.keySet()) {

            String nss = hashLoad.get(key)[0];
            String fmd = hashLoad.get(key)[1];

            if (userDB.getUser(nss) == null) {
                fingerIndex = new ArrayList<DPFPFingerIndex>();
                nameDB.put(nss, fingerIndex);
                if (userDB.addUser(nss) == null) {
                    nameDB.remove(nss);
                    LOGGER.warn("Error al insertar User a nameDB");
                }
            }

            UserDatabase.User user = userDB.getUser(nss);
            if (!nameDB.containsKey(nss)) {
                fingerIndex = new ArrayList<DPFPFingerIndex>();
                nameDB.put(nss, fingerIndex);
            }

            String[] bfmd = fmd.split("i");
            byte[] fmdBytes = new byte[bfmd.length - 1];
            for (int i = 1; i < bfmd.length; i++) {
                fmdBytes[i - 1] = (byte) Integer.parseInt(bfmd[i]);
            }

            DPFPTemplate template = DPFPGlobal.getTemplateFactory().createTemplate(fmdBytes);

            if (fingerIndex == null) {
                fingerIndex = nameDB.get(nss);
            }

            if (user.getTemplate(DPFPFingerIndex.LEFT_PINKY) == null) {
                user.setTemplate(DPFPFingerIndex.LEFT_PINKY, template);
                fingerIndex.add(DPFPFingerIndex.LEFT_PINKY);
            } else if (user.getTemplate(DPFPFingerIndex.LEFT_RING) == null) {
                user.setTemplate(DPFPFingerIndex.LEFT_RING, template);
                fingerIndex.add(DPFPFingerIndex.LEFT_RING);
            } else if (user.getTemplate(DPFPFingerIndex.LEFT_MIDDLE) == null) {
                user.setTemplate(DPFPFingerIndex.LEFT_MIDDLE, template);
                fingerIndex.add(DPFPFingerIndex.LEFT_MIDDLE);
            } else if (user.getTemplate(DPFPFingerIndex.LEFT_INDEX) == null) {
                user.setTemplate(DPFPFingerIndex.LEFT_INDEX, template);
                fingerIndex.add(DPFPFingerIndex.LEFT_INDEX);
            } else if (user.getTemplate(DPFPFingerIndex.LEFT_THUMB) == null) {
                user.setTemplate(DPFPFingerIndex.LEFT_THUMB, template);
                fingerIndex.add(DPFPFingerIndex.LEFT_THUMB);
            } else if (user.getTemplate(DPFPFingerIndex.RIGHT_THUMB) == null) {
                user.setTemplate(DPFPFingerIndex.RIGHT_THUMB, template);
                fingerIndex.add(DPFPFingerIndex.RIGHT_THUMB);
            } else if (user.getTemplate(DPFPFingerIndex.RIGHT_INDEX) == null) {
                user.setTemplate(DPFPFingerIndex.RIGHT_INDEX, template);
                fingerIndex.add(DPFPFingerIndex.RIGHT_INDEX);
            } else if (user.getTemplate(DPFPFingerIndex.RIGHT_MIDDLE) == null) {
                user.setTemplate(DPFPFingerIndex.RIGHT_MIDDLE, template);
                fingerIndex.add(DPFPFingerIndex.RIGHT_MIDDLE);
            } else if (user.getTemplate(DPFPFingerIndex.RIGHT_RING) == null) {
                user.setTemplate(DPFPFingerIndex.RIGHT_RING, template);
                fingerIndex.add(DPFPFingerIndex.RIGHT_RING);
            } else if (user.getTemplate(DPFPFingerIndex.RIGHT_PINKY) == null) {
                user.setTemplate(DPFPFingerIndex.RIGHT_PINKY, template);
                fingerIndex.add(DPFPFingerIndex.RIGHT_PINKY);
            } else {
                LOGGER.error(Constants.ERROR_NSS);
            }
            nameDB.put(nss, fingerIndex);
        }
    }

    public static BufferedImage toBufferedImage(Image src) {
        int w = src.getWidth(null);
        int h = src.getHeight(null);
        int type = BufferedImage.TYPE_INT_RGB; // other options
        BufferedImage dest = new BufferedImage(w, h, type);
        Graphics2D g2 = dest.createGraphics();
        g2.drawImage(src, 0, 0, null);
        g2.dispose();
        return dest;
    }

    public static String encodeToString(BufferedImage image, String type) {
        String imageString = null;
        String response = Constants.ERROR_TYPE;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        if (type.equals(Constants.PNG) || type.equals(Constants.JPG)) {
            final String tipo = type.equals(Constants.PNG) ? Constants.DATA_PNG : Constants.DATA_JPG;
            try {
                ImageIO.write(image, type, bos);
                byte[] imageBytes = bos.toByteArray();

                imageString = Base64.getEncoder().encodeToString(imageBytes);

                bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            imageString = imageString.replace("\n", "").replace("\r", "");
            response = tipo + imageString;
        }
        return response;
    }

    public static DPFPSample getSample(final String activeReader, final String prompt) throws InterruptedException {

        DPFPSample response = null;
        final LinkedBlockingQueue<DPFPSample> samples = new LinkedBlockingQueue<>();
        DPFPCapture capture = DPFPGlobal.getCaptureFactory().createCapture();
        capture.setReaderSerialNumber(activeReader);
        capture.setPriority(DPFPCapturePriority.CAPTURE_PRIORITY_LOW);
        capture.addDataListener(new DPFPDataListener() {
            public void dataAcquired(DPFPDataEvent e) {
                if (e != null && e.getSample() != null) {
                    try {
                        samples.put(e.getSample());
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        capture.addReaderStatusListener(new DPFPReaderStatusAdapter() {
            int lastStatus = DPFPReaderStatusEvent.READER_CONNECTED;

            public void readerConnected(DPFPReaderStatusEvent e) {
                if (lastStatus != e.getReaderStatus()) {
                    LOGGER.info(Constants.READER_CONNECT);
                }
                lastStatus = e.getReaderStatus();
            }

            public void readerDisconnected(DPFPReaderStatusEvent e) {
                if (lastStatus != e.getReaderStatus()) {
                    LOGGER.info(Constants.READER_DISCONNECT);
                }
                lastStatus = e.getReaderStatus();
            }
        });

        try {
            capture.startCapture();
            response = samples.take();
        } catch (RuntimeException e) {
            Thread.currentThread().stop();
        } finally {
            capture.stopCapture();
        }
        return response;
    }
}